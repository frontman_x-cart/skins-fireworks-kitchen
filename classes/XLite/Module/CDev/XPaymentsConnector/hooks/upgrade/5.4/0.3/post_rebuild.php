<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

return function ()
{
    $settings = \XLite\Module\CDev\XPaymentsConnector\Core\Settings::getInstance();

    $connectResult = $settings->testConnection();

    if ($settings::RESULT_FAILED !== $connectResult) {
        $settings->importPaymentMethods($connectResult);
    }
};
