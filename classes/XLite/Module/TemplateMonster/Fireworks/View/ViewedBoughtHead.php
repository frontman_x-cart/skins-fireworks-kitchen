<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\Fireworks\View;
/**
 *
 * @Decorator\Depend ("CDev\ProductAdvisor")
 *
 * @ListChild (list="modules.tabs.head", zone="customer", weight="700")
 */


class ViewedBoughtHead extends \XLite\Module\CDev\ProductAdvisor\View\ViewedBought
{
    /**
     * Return default template
     * See setWidgetParams()
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules.tabs.head.twig';
    }
}