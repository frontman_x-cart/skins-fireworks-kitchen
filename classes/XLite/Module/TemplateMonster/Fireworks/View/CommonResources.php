<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\Fireworks\View;

abstract class CommonResources extends \XLite\View\CommonResources implements \XLite\Base\IDecorator
{
    protected function getThemeFiles($adminZone = null)
    {
        $list = parent::getThemeFiles($adminZone);

        if (!(null === $adminZone ? \XLite::isAdminZone() : $adminZone)) {
            $list[static::RESOURCE_JS][] = 'js/bootstrap-tabcollapse.js';
            $list[static::RESOURCE_JS][] = 'js/jquery.collapser.js';
            $list[static::RESOURCE_JS][] = 'js/jquery.floating-label.js';
            $list[static::RESOURCE_JS][] = 'js/jquery.path.js';
            $list[static::RESOURCE_JS][] = 'js/jquery.fly.js';
            $list[static::RESOURCE_JS][] = 'js/utils.js';
            $list[static::RESOURCE_JS][] = 'js/header.js';
            $list[static::RESOURCE_JS][] = 'js/tab.min.js';
            $list[static::RESOURCE_JS][] = 'js/footer.js';

            $list[static::RESOURCE_CSS][] = 'css/tab.css';
            $list[static::RESOURCE_CSS][] = [
                'url' => '//fonts.googleapis.com/css?family='
                    . urlencode('Ubuntu:300,400,500,700')
                    . '&subset='
                    . urlencode('latin'),
                'media' => 'not print',
            ];
            $list[static::RESOURCE_CSS][] = [
                'url' => '//fonts.googleapis.com/css?family='
                    . urlencode('Play:700')
                    . '&subset='
                    . urlencode('latin'),
                'media' => 'not print',
            ];
            $list[static::RESOURCE_CSS][] = [
                'url' => '//fonts.googleapis.com/css?family=Roboto+Condensed'
                    . '&subset='
                    . urlencode('latin'),
                'media' => 'not print',
            ];

        }

        return $list;
    }
}
