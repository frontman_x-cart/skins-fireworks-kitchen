<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\Fireworks\View;
/**
 *
 * @Decorator\Depend ("CDev\FeaturedProducts")
 *
 * @ListChild (list="modules.tabs.head", zone="customer", weight="100")
 */


class FeaturedProductsHead extends \XLite\Module\CDev\FeaturedProducts\View\Customer\FeaturedProducts
{
    /**
     * Return default template
     * See setWidgetParams()
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules.tabs.head.twig';
    }
}