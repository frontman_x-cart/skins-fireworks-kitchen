<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\Fireworks\View;
/**
 *
 * @Decorator\Depend ("CDev\Bestsellers")
 *
 * @ListChild (list="modules.tabs.head", zone="customer", weight="200")
 */


class BestsellersHead extends \XLite\Module\CDev\Bestsellers\View\Bestsellers
{
    /**
     * Return default template
     * See setWidgetParams()
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules.tabs.head.twig';
    }
}