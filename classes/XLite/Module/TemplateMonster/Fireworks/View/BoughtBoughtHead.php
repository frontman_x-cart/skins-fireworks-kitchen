<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\Fireworks\View;
/**
 *
 * @Decorator\Depend ("CDev\ProductAdvisor")
 *
 * @ListChild (list="modules.tabs.head", zone="customer", weight="800")
 */


class BoughtBoughtHead extends \XLite\Module\CDev\ProductAdvisor\View\BoughtBought
{
    /**
     * Return default template
     * See setWidgetParams()
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules.tabs.head.twig';
    }
}