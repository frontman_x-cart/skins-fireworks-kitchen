<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\Fireworks\Module\ProductAdvisor;

/**
 * 'Customers who viewed this product bought' widget
 * @Decorator\Depend ("CDev\ProductAdvisor")
 *
 */
abstract class ViewedBought extends \XLite\Module\CDev\ProductAdvisor\View\ViewedBought implements \XLite\Base\IDecorator
{
    /**
     * Return default template
     * See setWidgetParams()
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules.tabs.content.twig';
    }
    /**
     * Register the CSS classes for this block
     *
     * @return string
     */
    protected function getBlockId()
    {
        return 'block-viewed-bought';
    }
    /**
     * Define widget parameters
     *
     * @return void
     */
    protected function defineWidgetParams()
    {
        parent::defineWidgetParams();
        $this->widgetParams[self::PARAM_DISPLAY_MODE]->setValue(self::DISPLAY_MODE_GRID);
        $this->widgetParams[self::PARAM_WIDGET_TYPE]->setValue(self::WIDGET_TYPE_CENTER);
    }

    /**
     * Initialize widget (set attributes)
     *
     * @param array $params Widget params
     *
     * @return void
     */
    public function setWidgetParams(array $params)
    {
        parent::setWidgetParams($params);
        $this->widgetParams[self::PARAM_DISPLAY_MODE]->setValue(self::DISPLAY_MODE_GRID);
        $this->widgetParams[self::PARAM_WIDGET_TYPE]->setValue(self::WIDGET_TYPE_CENTER);
    }
}
