<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\Fireworks;

use Includes\Utils\Module\Manager;

/**
 * Development module that simplifies the process of implementing design changes
 *
 */
abstract class Main extends \XLite\Module\AModuleSkin
{
    /**
     * Author name
     *
     * @return string
     */
    public static function getAuthorName()
    {
        return 'TemplateMonster.com';
    }

    /**
     * Module name
     *
     * @return string
     */
    public static function getModuleName()
    {
        return 'Fireworks';
    }

    /**
     * Get module major version
     *
     * @return string
     */
    public static function getMajorVersion()
    {
        return '5.4';
    }

    /**
     * Get minor core version which is required for the module activation
     *
     * @return string
     */
    public static function getMinorRequiredCoreVersion()
    {
        return '0';
    }
    /**
     * Module version
     *
     * @return string
     */
    public static function getMinorVersion()
    {
        return '0';
    }

    /**
     * Get module build number (4th number in the version)
     *
     * @return string
     */
    public static function getBuildVersion()
    {
        return '0';
    }

    /**
     * Module description
     *
     * @return string
     */
    public static function getDescription()
    {
        return 'This bright high-contrast skin with modern straight cornered flat design will become a good fit for stores selling fireworks and car accessories.';
    }

    /**
     * Determines if we need to show settings form link
     *
     * @return boolean
     */
    public static function showSettingsForm()
    {
        return false;
    }


    /**
     * Register the module skins.
     *
     * @return array
     */
    public static function getSkins()
    {
        return array(

            \XLite::CUSTOMER_INTERFACE => array(

                'TemplateMonster_Fireworks' .LC_DS. 'customer',

            ),
        );
    }

    public static function getLayoutTypes()
    {
        return array(
            \XLite\Core\Layout::LAYOUT_ONE_COLUMN,
            \XLite\Core\Layout::LAYOUT_TWO_COLUMNS_RIGHT,
            \XLite\Core\Layout::LAYOUT_TWO_COLUMNS_LEFT,
            \XLite\Core\Layout::LAYOUT_THREE_COLUMNS
        );
    }

    /**
     * Determines if some module is enabled
     *
     * @return boolean
     */
    public static function isModuleEnabled($name)
    {
        list($author, $name) = explode('\\', $name);

        return Manager::getRegistry()->isModuleEnabled($author, $name);
    }

    protected static function moveTemplatesInLists()
    {
        $templates = [
            'layout/content/main.location.twig'                                         => [
                    ['layout.main', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['layout.main', 360, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
            ],
            'product/details/parts/common.price.twig'                                         => [
                    ['product.details.page.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.page.info.right', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
            ],
            'product/details/parts/common.notify-on-price-drop.twig'                                         => [
                    ['product.details.page.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.page.info.right', 150, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
            ],
            'product/details/parts/common.info.buttons.twig'                                         => [
                    ['product.details.page.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
            ],
            'authorization/parts/field.links.twig' => [
                static::TO_ADD    => [
                    ['customer.signin.popup.fields', 500, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.right.twig'                                           => [
                static::TO_DELETE => [
                    ['layout.header', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                      ['layout.header.bar', 50, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.bar.twig'                                               => [
                static::TO_DELETE => [
                    ['layout.header', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header', 50, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.bar.links.logged.orders.twig'             => [
                static::TO_DELETE => [
                    ['layout.header.bar.links.logged', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.bar.search.twig'                                      => [
                static::TO_DELETE => [
                    ['layout.header.bar', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/mobile_header_parts/account_menu.twig'                       => [
                static::TO_DELETE => [
                    ['layout.header.mobile.menu', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/mobile_header_parts/language_menu.twig'                      => [
                static::TO_DELETE => [
                    ['layout.header.mobile.menu', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/mobile_header_parts/search_menu.twig'                        => [
                static::TO_DELETE => [
                    ['layout.header.mobile.menu', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/mobile_header_parts/slidebar_menu.twig'                      => [
                static::TO_DELETE => [
                    ['layout.header.mobile.menu', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header.mobile.menu', 2000, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.labels.twig'                               => [
                static::TO_ADD    => [
                    ['product.details.page.image', 17, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.drag-n-drop-handle.twig'                   => [
                static::TO_DELETE => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.list.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.small_thumbnails.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/stock/label.twig'                                          => [
                static::TO_ADD    => [
                    ['itemsList.product.list.customer.info', 32, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.product-thumbnail.twig'   => [
                static::TO_ADD => [
                    ['itemsList.product.grid.customer.info', 10, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.product-name.twig'                         => [
                static::TO_ADD => [
                    ['itemsList.product.grid.customer.info', 30, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.product-price.twig'                         => [
                static::TO_DELETE => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD => [
                    ['itemsList.product.grid.customer.info', 20, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/center/list/parts/common.product-name.twig'             => [
                static::TO_ADD => [
                    ['itemsList.product.list.customer.info', 20, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.field-select-product.twig'                 => [
                static::TO_DELETE => [
                    ['itemsList.product.table.customer.columns', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/parts/common.briefDescription.twig'                        => [
                static::TO_DELETE => [
                    ['product.details.page.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['product.details.page.info', 18, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/parts/common.stock.twig'                                   => [
                static::TO_DELETE => [
                    ['product.details.page.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['product.details.page.info', 16, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],

            'items_list/product/parts/grid.button-add2cart-wrapper.twig'                => [
                static::TO_ADD    => [
                    ['itemsList.product.grid.customer.info', 60, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/parts/common.product-added.twig'                           => [
                static::TO_DELETE => [
                    ['product.details.page.info.buttons-added', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.quicklook.info.buttons-added', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['product.details.page.image', 10, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.quicklook.image', 5, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
        ];


        if (static::isModuleEnabled('XC\FreeShipping')) {
            $templates += [
                'modules/XC/FreeShipping/free_ship.label.twig'                              => [
                    static::TO_DELETE => [
                        ['product.details.page.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.details.page.info', 17, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
            ];
        }
        if (static::isModuleEnabled('CDev\Paypal')) {
            $templates += [
                'modules/CDev/Paypal/banner/ProductDetailsPages/nearAddToCartButton.twig'                                         => [
                    ['product.details.page.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.page.info.right', 300, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }


        if (static::isModuleEnabled('XC\FastLaneCheckout')) {
            $templates += [
                'modules/XC/FastLaneCheckout/checkout_fastlane/header/title.twig'      => [
                    static::TO_DELETE => [
                        ['checkout_fastlane.header.left', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
            ];
        }

        if (static::isModuleEnabled('XC\Reviews')) {
            $templates += [
                'modules/XC/Reviews/product.items_list.rating.twig' => [
                    static::TO_DELETE    => [
                        ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['itemsList.product.grid.customer.info', 40, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
            ];
        }

        if (static::isModuleEnabled('CDev\Sale')) {
            $templates += [
                'modules/CDev/Sale/label.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price.tail', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.plain_price', 10, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
                'modules/CDev/Sale/details/price.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
                'modules/CDev/Sale/details/sale_price.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price.tail.sale_price.text', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.plain_price.sale_price.text', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
                'modules/CDev/Sale/details/you_save.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price.tail.sale_price.text', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.plain_price.sale_price.text', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
                'modules/CDev/Sale/details/label.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price.tail.sale_price.text', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.plain_price', 30, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
            ];
        }

        if (static::isModuleEnabled('CDev\SocialLogin')) {
            $templates['modules/CDev/SocialLogin/signin/signin.checkout.social.twig'] = [
                static::TO_ADD    => [
                    ['signin-anonymous-title', 5, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('XC\MultiVendor')) {
            $templates['modules/XC/MultiVendor/product/details/parts/vendor.twig'] = [
                static::TO_DELETE => [
                    ['product.details.page', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.quicklook', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['product.details.page.info', 11, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.quicklook.info', 11, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('Amazon\PayWithAmazon')) {
            $templates['modules/Amazon/PayWithAmazon/login/signin/signin.checkout.twig'] = [
                static::TO_ADD    => [
                    ['signin-anonymous-title', 5, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('QSL\MyWishlist')) {
            $templates['modules/QSL/MyWishlist/items_list/product/parts/common.close-button.twig'] = [
                static::TO_DELETE => [
                    ['itemsList.product.table.customer.columns', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.table.customer.columns', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
            $templates['modules/QSL/MyWishlist/header/header.bar.wishlist.twig'] = [
                static::TO_DELETE => [
                    ['layout.header.bar', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('XC\ProductComparison') || \XLite\Core\Config::getInstance()->General->enable_add2cart_button_grid){
            $templates['items_list/product/parts/grid.buttons-container.twig'] = [
                static::TO_DELETE   => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }


        return $templates;
    }

    /**
     * @return array
     */
    protected static function moveClassesInLists()
    {
        $classes_list = [
            'XLite\View\Subcategories' => [
                ['center.bottom', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ['layout.main', 380, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
            ],
        ];

        if (static::isModuleEnabled('QSL\Banner')) {
            $classes_list['XLite\Module\QSL\Banner\View\Customer\BannerSectionWideTop'] = [
                static::TO_DELETE => [
                    ['layout.main', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.main', 350, \XLite\Model\ViewList::INTERFACE_CUSTOMER]
                ],
            ];
        }


        if (static::isModuleEnabled('XC\NextPreviousProduct')) {
            $classes_list['XLite\Module\XC\NextPreviousProduct\View\Product\Details\Customer\NextPreviousProduct'] = [
                static::TO_ADD    => [
                    ['product.details.page', 10, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('QSL\MyWishlist')) {
            $classes_list['XLite\Module\QSL\MyWishlist\View\AddButtonItemsList'] = [
                static::TO_DELETE => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.list.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.table.customer.columns', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.grid.customer.tail', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.list.customer.tail', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.table.customer.columns', 43, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('XC\ProductComparison')) {
            $classes_list['XLite\Module\XC\ProductComparison\View\AddToCompare\Product'] = [
                static::TO_ADD => [
                    ['product.details.quicklook.info.buttons.cart-buttons', 120, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.quicklook.info.buttons-added.cart-buttons', 129, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
            $classes_list['XLite\Module\XC\ProductComparison\View\AddToCompare\Products'] = [
                static::TO_ADD    => [
                    ['itemsList.product.grid.customer.tail', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.list.customer.tail', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.table.customer.marks', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('CDev\FeaturedProducts')){
            $classes_list['XLite\Module\CDev\FeaturedProducts\View\Customer\FeaturedProducts'] = array(
                array('center.bottom', 'customer'),
                array('modules.tabs.content', 100, 'customer'),
            );
        }

        if (static::isModuleEnabled('CDev\Bestsellers')){
            $classes_list['XLite\Module\CDev\Bestsellers\View\Bestsellers'] = array(
                static::TO_DELETE => array(
                    array('sidebar.single', \XLite\Model\ViewList::INTERFACE_CUSTOMER),
                    array('sidebar.first', \XLite\Model\ViewList::INTERFACE_CUSTOMER),
                    array('center.bottom', \XLite\Model\ViewList::INTERFACE_CUSTOMER),
                ),
                static::TO_ADD => array(
                    array('modules.tabs.content', 200, 'customer'),
                )
            );
        }

        if (static::isModuleEnabled('CDev\ProductAdvisor')){
            $classes_list['XLite\Module\CDev\ProductAdvisor\View\NewArrivals'] = array(
                static::TO_DELETE => array(
                    array('sidebar.single', \XLite\Model\ViewList::INTERFACE_CUSTOMER),
                    array('sidebar.second', \XLite\Model\ViewList::INTERFACE_CUSTOMER),
                    array('center.bottom', \XLite\Model\ViewList::INTERFACE_CUSTOMER),
                ),
                static::TO_ADD => array(
                    array('modules.tabs.content', 400, 'customer'),
                )
            );
            $classes_list['XLite\Module\CDev\ProductAdvisor\View\ViewedBought'] = array(
                array('center.bottom', 'customer'),
                array('modules.tabs.content', 700, 'customer'),
            );
            $classes_list['XLite\Module\CDev\ProductAdvisor\View\BoughtBought'] = array(
                array('center.bottom', 'customer'),
                array('modules.tabs.content', 800, 'customer'),
            );
        }

        return $classes_list;
    }

    /**
     * Returns image sizes
     *
     * @return array
     */
    public static function getImageSizes()
    {
        return [
            \XLite\Logic\ImageResize\Generator::MODEL_PRODUCT => [
                'SBSmallThumbnail' => [100, 120],
                'XSThumbnail'      => [58, 70],
            ],
        ];
    }
}
