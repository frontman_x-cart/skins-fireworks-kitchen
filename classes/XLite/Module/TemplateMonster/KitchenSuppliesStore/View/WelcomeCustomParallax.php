<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Welcome page
 *
 * @ListChild (list="layout.footer", zone="customer", weight="1")
 */
class WelcomeCustomParallax extends \XLite\View\AView
{
    /**
     * Return list of targets allowed for this widget
     *
     * @return array
     */
    public static function getAllowedTargets()
    {
        $result = parent::getAllowedTargets();
        $result[] = 'main';

        return $result;
    }


    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'layout/content/welcome_custom_parallax.twig';
    }

    /**
     * Get a list of CSS files required to display the widget properly
     *
     * @return array
     */
     public function getCSSFiles()
     {
         $list = parent::getCSSFiles();

         $list[] = 'css/welcome_custom_parallax.css';

         return $list;
     }

    /**
     * Register JS files
     *
     * @return array
     */
     public function getJSFiles()
     {
        $list = parent::getJSFiles();
        //$list[] = 'js/parallax/device.min.js';
        //$list[] = 'js/parallax/tm-media-parallax.js';
        //$list[] = 'js/welcome_custom_parallax.js';

        return $list;
     }

    /**
     * Check widget visibility
     *
     * @return boolean
     */
    protected function isVisible()
    {
        return parent::isVisible() && !\XLite\Core\Request::getInstance()->page;
    }
}
