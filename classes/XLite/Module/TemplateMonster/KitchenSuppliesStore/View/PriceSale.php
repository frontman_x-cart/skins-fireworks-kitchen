<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Viewer
 * 
 * @Decorator\Depend("CDev\Sale")
 */
abstract class PriceSale extends \XLite\View\Price implements \XLite\Base\IDecorator
{
    /**
     * Return the specific sale price label info
     *
     * @return array
     */
    public function getSalePriceLabel()
    {
        if (!isset($this->salePriceLabel)) {
            if ($this->participateSale()) {
                /*$label = '-' . $this->getSalePercent() . '%';*/
                $label = static::t('SALE!');
                $this->salePriceLabel = array(
                    'green sale-price' => $label,
                );

                \XLite\Module\CDev\Sale\Core\Labels::addLabel($this->getProduct(), $this->salePriceLabel);
            }
        }

        return $this->salePriceLabel;
    }
}
