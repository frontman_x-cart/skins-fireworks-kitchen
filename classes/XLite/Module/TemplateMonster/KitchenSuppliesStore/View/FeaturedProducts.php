<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Featured products widget
 *
 * ListChild (list="center.bottom", zone="customer", weight="300")
 * 
 * @Decorator\Depend ("CDev\FeaturedProducts")
 */
class FeaturedProducts extends \XLite\Module\CDev\FeaturedProducts\View\Customer\FeaturedProducts implements \XLite\Base\IDecorator
{
    /**
     * Check if widget is visible
     *
     * @return boolean
     */
    protected function isVisible()
    {
        $result = parent::isVisible()
            || 'welcome_tab_feat' == $this->viewListName;

        if ('main' == \XLite\Core\Request::getInstance()->target) {
            $result = 'center.bottom' != $this->viewListName
                && $result;
        }

        return $result;
    }  
}
