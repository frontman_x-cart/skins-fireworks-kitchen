<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Welcome page
 *
 * ListChild (list="center", zone="customer")
 */
class Welcome extends \XLite\View\Welcome implements \XLite\Base\IDecorator
{
    /**
     * Return description with postprocessing WEB LC root constant
     *
     * @return string
     */
    protected function getFrontPageDescription()
    {
        return $this->getCategory()->getViewDescription();
    }
}
