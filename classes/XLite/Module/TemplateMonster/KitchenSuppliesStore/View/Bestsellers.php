<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Bestsellers widget
 * 
 * ListChild (list="center.bottom", zone="customer", weight="400")
 * 
 * @Decorator\Depend("CDev\Bestsellers")
 */
class Bestsellers extends \XLite\Module\CDev\Bestsellers\View\Bestsellers implements \XLite\Base\IDecorator
{
    /**
     * Define widget parameters
     *
     * @return void
     */
    public function setWidgetParams(array $params)
    {
        parent::setWidgetParams($params);

        $this->widgetParams[static::PARAM_DISPLAY_MODE]->setValue(\XLite\Core\Config::getInstance()->CDev->FeaturedProducts->featured_products_look);
    }

    /**
     * Check if widget is visible
     *
     * @return boolean
     */
    protected function isVisible()
    {
        $result = parent::isVisible()
            || 'welcome_tab_best' == $this->viewListName;

        if ('main' == \XLite\Core\Request::getInstance()->target
            && self::WIDGET_TYPE_CENTER == $this->getParam(self::PARAM_WIDGET_TYPE)
        ) {
            $result = 'center.bottom' != $this->viewListName
                && $result;
        }

        return $result;
    }

}
