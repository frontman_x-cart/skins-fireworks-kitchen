<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Abstract widget
 */
abstract class AView extends \XLite\View\AView implements \XLite\Base\IDecorator
{
    /**
     * Return theme common files
     *
     * @param boolean|null $adminZone
     *
     * @return array
     */
    protected function getThemeFiles($adminZone = null)
    {
        $list = parent::getThemeFiles();
        
        if (!(is_null($adminZone) ? \XLite::isAdminZone() : $adminZone)) {
            $list[static::RESOURCE_CSS][] = array(
                'file'  => 'css/altskin.less',
                'merge' => 'bootstrap/css/bootstrap.less',
            );
            $list[static::RESOURCE_CSS][] = 'css/altskin.css';
            $list[static::RESOURCE_JS][] = 'js/altskin.js';            
        }

        return $list;
    }

    /**
     * Get invoice logo
     *
     * @return string
     */
    public function getInvoiceLogo()
    {        
        if (\XLite\Core\Config::getInstance()->CDev->SimpleCMS->logo === '') { 
            $url = \XLite\Core\Layout::getInstance()->getResourceWebPath('images/logo_invoice.png', \XLite\Core\Layout::WEB_PATH_OUTPUT_URL, \XLite::CUSTOMER_INTERFACE);
        } else {
            $url = parent::getInvoiceLogo();
        }               

        return $url;        
    }    
    
    
}
