<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Sidebar categories list
 *
 * _ListChild (list="sidebar.first", zone="customer", weight="100")
 */
class TopCategories extends \XLite\View\TopCategories implements \XLite\Base\IDecorator
{

    /**
     * Get widge title
     *
     * @return string
     */
    protected function getHead()
    {
        return '';
    }

    /**
     * Register the CSS classes for this block
     *
     * @return string
     */
    protected function getBlockClasses()
    {
        return parent::getBlockClasses() . ' horizontal';
    }
}
