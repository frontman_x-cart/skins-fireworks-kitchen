<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Category widget
 *
 * ListChild (list="center", zone="customer")
 */
class Category extends \XLite\View\Category implements \XLite\Base\IDecorator
{
    /**
     * Return list of targets allowed for this widget
     *
     * @return array
     */
    public static function getAllowedTargets()
    {
        $result = parent::getAllowedTargets();
        $anti_result[] = 'main';

        return array_diff($result, $anti_result);
    }

    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'main' == \XLite\Core\Request::getInstance()->target
          ? 'layout/content/category_description.twig'
          : 'layout/content/category_description_with_image.twig';
    }

    /**
     * Return number of products associated with the category
     *
     * @return integer
     */
    public function getCategoryProductsCount($categoryId)
    {
        $cnd = new \XLite\Core\CommonCell();

        // Main condition for this search
        $cnd->{\XLite\Model\Repo\Product::P_CATEGORY_ID} = $categoryId;

        if ('directLink' !== \XLite\Core\Config::getInstance()->General->show_out_of_stock_products
            && !\XLite::isAdminZone()
        ) {
            $cnd->{\XLite\Model\Repo\Product::P_INVENTORY} = false;
        }

        return \XLite\Core\Database::getRepo('XLite\Model\Product')->search($cnd, true);
    }
}
