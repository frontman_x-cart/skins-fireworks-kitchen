<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * New arrivals block widget
 *
 * ListChild (list="sidebar.single", zone="customer", weight="160")
 * ListChild (list="sidebar.second", zone="customer", weight="110")
 * 
 * @Decorator\Depend("CDev\ProductAdvisor")
 */
class NewArrivals extends \XLite\Module\CDev\ProductAdvisor\View\NewArrivals implements \XLite\Base\IDecorator
{
    /**
     * Define widget parameters
     *
     * @return void
     */
    protected function defineWidgetParams()
    {
        parent::defineWidgetParams();
        
        $this->widgetParams[static::PARAM_DISPLAY_MODE]->setValue(\XLite\Core\Config::getInstance()->CDev->FeaturedProducts->featured_products_look);
        $this->widgetParams[self::PARAM_WIDGET_TYPE]->setValue(self::WIDGET_TYPE_CENTER);
    }

    /**
     * Define widget parameters
     *
     * @return void
     */
    public function setWidgetParams(array $params)
    {
        parent::setWidgetParams($params);

        $this->widgetParams[static::PARAM_DISPLAY_MODE]->setValue(\XLite\Core\Config::getInstance()->CDev->FeaturedProducts->featured_products_look);
        $this->widgetParams[self::PARAM_WIDGET_TYPE]->setValue(self::WIDGET_TYPE_CENTER);
    }

    /**
     * Get limit condition
     *
     * @return \XLite\Core\CommonCell
     */
    protected function getLimitCondition()
    {
        $cnd = $this->getSearchCondition();

        $cnd->{\XLite\Model\Repo\ARepo::P_LIMIT} = [
            0,
            \XLite\Core\Config::getInstance()->CDev->ProductAdvisor->na_max_count_in_block,
        ];

        return $cnd;
    }    
}
