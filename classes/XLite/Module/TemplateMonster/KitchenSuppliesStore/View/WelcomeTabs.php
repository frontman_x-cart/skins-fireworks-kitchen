<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Welcome page tabs
 *
 * @ListChild (list="center.bottom", zone="customer", weight="200")
 */
class WelcomeTabs extends \XLite\View\AView
{
    /**
     * Tabs (cache)
     *
     * @var array
     */
    protected $tabs;
  
    /**
     * Return list of targets allowed for this widget
     *
     * @return array
     */
    public static function getAllowedTargets()
    {
        $result = parent::getAllowedTargets();
        $result[] = 'main';

        return $result;
    }

    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'layout/content/welcome_tabs.twig';
    }

    /**
     * Check widget visibility
     *
     * @return boolean
     */
    protected function isVisible()
    {
        return parent::isVisible()
        && !\XLite\Core\Request::getInstance()->page
        && !$this->isAJAX(); // without this can appear in terms-conditions popup
    }

    // {{{ Tabs

    /**
     * Compare tabs 
     * 
     * @param array $a Tab 1
     * @param array $b tab 2
     *  
     * @return integer
     */
    public function compareTabs(array $a, array $b)
    {
        if ($a['weight'] == $b['weight']) {
            $result = 0;

        } elseif ($a['weight'] > $b['weight']) {
            $result = 1;

        } else {
            $result = -1;
        }

        return $result;
    }

    /**
     * Get tabs
     *
     * @return array
     */
    protected function getTabs()
    {
        if (!isset($this->tabs)) {
            $list = $this->defineTabs();
            $i = 0;
            foreach ($list as $k => $data) {
                $id = isset($data['id'])
                    ? $data['id']
                    : preg_replace('/\W+/Ss', '-', strtolower($k));

                $list[$k] = array(
                    'index'  => $i,
                    'id'     => 'welcome-page-tab-' . $id,
                    'name'   => is_array($data) && !empty($data['name'])
                        ? $data['name']
                        : static::t($k),
                    'weight' => isset($data['weight']) ? $data['weight'] : $i,
                );

                if (is_string($data)) {
                    $list[$k]['template'] = $data;

                } elseif (is_array($data) && !empty($data['template'])) {
                    $list[$k]['template'] = $data['template'];

                } elseif (is_array($data) && !empty($data['list'])) {
                    $list[$k]['list'] = $data['list'];

                } elseif (is_array($data) && !empty($data['widget'])) {
                    $parameters = array(
                        'product' => $this->getProduct(),
                    );
                    if (!empty($data['parameters'])) {
                        $parameters += $data['parameters'];
                    }
                    $list[$k]['widgetObject'] = $this->getWidget($parameters, $data['widget']);
                    unset($data['widget']);

                } else {
                    unset($list[$k]);
                }

                $i++;
            }

            $this->tabs = $list;
            //uasort($this->tabs, array($this, 'compareTabs'));
        }

        return $this->tabs;
    }

    /**
     * Define tabs
     *
     * @return array
     */
    protected function defineTabs()
    {
        $list = array();

        $this->definePopularTab($list);
        $this->defineNewArrivalsTab($list);
        $this->defineBestSellersTab($list);

        return $list;
    }

    /**
     * Define the 'Popular' tab
     *
     * @param array $list
     */
    protected function definePopularTab(&$list)
    {
        if (\XLite\Module\TemplateMonster\KitchenSuppliesStore\Main::isModuleEnabled('CDev\FeaturedProducts')
            && \XLite\Module\CDev\FeaturedProducts\View\Customer\FeaturedProducts::getInstance()->isVisible()
        ) {
            $list['Popular'] = array(
                'list' => 'welcome_tab_feat',
                'weight' => '1',
            );
        }
    }

    /**
     * Define the 'New Arrivals' tab
     *
     * @param array $list
     */
    protected function defineNewArrivalsTab(&$list)
    {
        if (\XLite\Module\TemplateMonster\KitchenSuppliesStore\Main::isModuleEnabled('CDev\ProductAdvisor')
            && \XLite\Core\Config::getInstance()->CDev->ProductAdvisor->na_enabled
            && \XLite\Module\CDev\ProductAdvisor\View\NewArrivals::getInstance()->isVisible() 
        ) {
            $list['New arrivals'] = array(
                'list' => 'welcome_tab_new',
                'weight' => '2',
            );
        }
    }
    /**
     * Define the 'Bessellers' tab
     *
     * @param array $list
     */
    protected function defineBestSellersTab(&$list)
    {
        if (\XLite\Module\TemplateMonster\KitchenSuppliesStore\Main::isModuleEnabled('CDev\Bestsellers')) {
            $list['Best Sellers'] = array(
                'list' => 'welcome_tab_best',
                'weight' => '3',
            );
        }
    }
    
    /**
     * Get tab class
     *
     * @param array $tab Tab
     *
     * @return string
     */
    protected function getTabClass(array $tab)
    {
        return $this->isTabActive($tab) ? 'active' : '';
    }

    /**
     * Get tab container style
     *
     * @param array $tab Tab
     *
     * @return string
     */
    protected function getTabStyle(array $tab)
    {
        return 'fade' . ($this->isTabActive($tab) ? 'in active' : '');
    }

    /**
     * Check tab activity
     *
     * @param array $tab Tab info cell
     *
     * @return boolean
     */
    protected function isTabActive(array $tab)
    {
        return 0 === $tab['index'];
    }

    // }}}
}
