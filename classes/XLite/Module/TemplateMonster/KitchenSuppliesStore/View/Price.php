<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Details
 * 
 * @Decorator\Depend("CDev\MarketPrice")
 */
abstract class Price extends \XLite\View\Price implements \XLite\Base\IDecorator
{
    /**
     * Return the specific market price label info
     *
     * @return array
     */
    public function getMarketPriceLabel()
    {
        if (is_null($this->marketPriceLabel) && $this->isShowMarketPrice()) {
            $percent = 0;

            if ($this->getProduct()->getMarketPrice()) {
                $percent = min(99, round(($this->getSaveDifference() / $this->getProduct()->getMarketPrice()) * 100));
            }

            if (0 < $percent) {
                $this->marketPriceLabel['green market-price'] = static::t('SALE!'); /* '-'.$percent . '%' . static::t('less'); */
            }

            \XLite\Module\CDev\MarketPrice\Core\Labels::addLabel($this->getProduct(), $this->marketPriceLabel);
        }

        return $this->marketPriceLabel;
    }
}
