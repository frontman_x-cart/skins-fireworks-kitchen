<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Welcome page
 *
 * @ListChild (list="layout.footer", zone="customer", weight="2")
 */
class WelcomeCustomBottom extends \XLite\View\AView
{
    /**
     * Return list of targets allowed for this widget
     *
     * @return array
     */
    public static function getAllowedTargets()
    {
        $result = parent::getAllowedTargets();
        $result[] = 'main';

        return $result;
    }


    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'layout/content/welcome_custom_bottom.twig';
    }

    /**
     * Get a list of CSS files required to display the widget properly
     *
     * @return array
     */
     public function getCSSFiles()
     {
         $list = parent::getCSSFiles();

         $list[] = 'css/welcome_custom_bottom.css';

         return $list;
     }

    /**
     * Check widget visibility
     *
     * @return boolean
     */
    protected function isVisible()
    {
        return parent::isVisible() && !\XLite\Core\Request::getInstance()->page;
    }
}
