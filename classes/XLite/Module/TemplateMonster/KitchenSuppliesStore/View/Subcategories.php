<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore\View;

/**
 * Subcategories list
 *
 * ListChild (list="center.bottom", zone="customer", weight="100")
 */
class Subcategories extends \XLite\View\Subcategories implements \XLite\Base\IDecorator
{

}
