<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2016 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace XLite\Module\TemplateMonster\KitchenSuppliesStore;

use Includes\Utils\Module\Manager;

/**
 * Main module
 */
abstract class Main extends \XLite\Module\AModuleSkin
{
    /**
     * Author name
     *
     * @return string
     */
    public static function getAuthorName()
    {
        return 'TemplateMonster.com';
    }

    /**
     * Module name
     *
     * @return string
     */
    public static function getModuleName()
    {
        return 'Kitchen Supplies Store';
    }

    /**
     * Module description
     *
     * @return string
     */
    public static function getDescription()
    {
        return 'Kitchen Supplies Store Theme';
    }

    /**
     * Get module major version
     *
     * @return string
     */
    public static function getMajorVersion()
    {
        return '5.4';
    }

    /**
     * Module version
     *
     * @return string
     */
    public static function getMinorVersion()
    {
        return '0';
    }

    /**
     * Get module build number (4th number in the version)
     *
     * @return string
     */
    public static function getBuildVersion()
    {
        return '0';
    }

    /**
     * Get minor core version which is required for the module activation
     *
     * @return string
     */
    public static function getMinorRequiredCoreVersion()
    {
        return '0';
    }

    /**
     * Get the module skins list to register in layout.
     *
     * @return string
     */
    public static function getSkins()
    {
        return array(
            \XLite::CUSTOMER_INTERFACE => array('TemplateMonster_KitchenSuppliesStore/customer'),
        );
    }

    /**
     * Returns layout types
     *
     * @return array
     */
    public static function getLayoutTypes()
    {
        return array(
            \XLite\Core\Layout::LAYOUT_ONE_COLUMN,
            \XLite\Core\Layout::LAYOUT_TWO_COLUMNS_RIGHT,
            \XLite\Core\Layout::LAYOUT_TWO_COLUMNS_LEFT,
            \XLite\Core\Layout::LAYOUT_THREE_COLUMNS
        );
    }

    /**
     * Determines if some module is enabled
     *
     * @return boolean
     */
    public static function isModuleEnabled($name)
    {
        list($author, $name) = explode('\\', $name);

        return Manager::getRegistry()->isModuleEnabled($author, $name);
    }

    /**
     * @return array
     */
    protected static function moveTemplatesInLists()
    {
        $templates = [
            'layout/header/top_menu.twig'                                               => [
                static::TO_DELETE => [
                    ['layout.header',\XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header.line1', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.logo.twig'                                               => [
                static::TO_DELETE => [
                    ['layout.header', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header.line2', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.bar.links.logged.orders.twig'                                                         => [
                static::TO_DELETE => [
                    ['layout.header.bar.links.logged', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.right.search.twig'                   => [
                static::TO_DELETE => [
                    ['layout.header.right', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['layout.header.right.mobile', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.right.twig'                                               => [
                static::TO_DELETE => [
                    ['layout.header', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header.line2', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],

            'layout/header/header.bar.twig'                                               => [
                static::TO_DELETE => [
                    ['layout.header', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header.line2', 300, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/header.bar.search.twig'                                      => [
                static::TO_DELETE => [
                    ['layout.header.bar', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD => [
                    ['layout.header.line2', 150, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],                
            ],
            'layout/header/header.bar.checkout.logos.twig'                                      => [
                static::TO_DELETE => [
                    ['layout.header', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD => [
                    ['layout.header.line2', 300, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],                
            ],
            'layout/header/mobile_header_parts/account_menu.twig'                       => [
                static::TO_DELETE => [
                    ['layout.header.mobile.menu', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'authorization/parts/field.links.twig' => [
                static::TO_ADD    => [
                    ['customer.signin.popup.fields', 500, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'layout/header/mobile_header_parts/slidebar_menu.twig'                      => [
                static::TO_DELETE    => [
                    ['layout.header.mobile.menu', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header.mobile.menu', 1, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.labels.twig'                               => [
                static::TO_DELETE => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                   ['itemsList.product.grid.customer.info.photo', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.drag-n-drop-handle.twig'                   => [
                static::TO_DELETE => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.list.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.small_thumbnails.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/stock/label.twig'                                          => [
                static::TO_DELETE    => [
                    ['itemsList.product.list.customer.info',  \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.grid.customer.info',  \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.list.customer.info',  \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.grid.customer.info', 29, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.list.customer.info', 32, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/common.product-price.twig'                           => [
                static::TO_DELETE => [
                    ['itemsList.product.list.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.list.customer.right_info', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.grid.customer.info',19, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'items_list/product/parts/grid.button-add2cart-wrapper.twig'                => [
                static::TO_DELETE => [
                    ['itemsList.product.grid.customer.tail', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.grid.customer.info', 22, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],            
            'items_list/product/parts/list.button-add2cart.twig'                           => [
                static::TO_DELETE => [
                    ['itemsList.product.list.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.list.customer.right_info', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/parts/common.briefDescription.twig'                        => [
                static::TO_ADD    => [
                    ['product.details.page.info', 18, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/parts/common.stock.twig'                                   => [
                static::TO_ADD    => [
                    ['product.details.page.info', 109, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/parts/page.image.twig'                          => [
                static::TO_DELETE => [
                    ['product.details.page', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['product.details.page.left_part', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/parts/page.info.twig' => [
                static::TO_DELETE => [
                    ['product.details.page', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['product.details.page.right_part', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'product/details/parts/common.product-added.twig'                           => [
                static::TO_ADD    => [
                    ['product.details.page.image', 10, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['product.details.quicklook.image', 5, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
        ];
        if (static::isModuleEnabled('XC\FreeShipping')) {
            $templates += [
                'modules/XC/FreeShipping/free_ship.label.twig'                              => [
                    static::TO_DELETE => [
                        ['product.details.page.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.details.page.info', 17, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
            ];
        }

        if (static::isModuleEnabled('XC\FastLaneCheckout')) {
            $templates += [
                'modules/XC/FastLaneCheckout/checkout_fastlane/header/title.twig'      => [
                    static::TO_DELETE => [
                        ['checkout_fastlane.header.left', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
            ];
        }


        if (static::isModuleEnabled('CDev\Sale')) {
            $templates += [
                'modules/CDev/Sale/label.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price.tail', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.plain_price', 10, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
                'modules/CDev/Sale/details/price.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
                'modules/CDev/Sale/details/sale_price.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price.tail.sale_price.text', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.plain_price.sale_price.text', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
                'modules/CDev/Sale/details/you_save.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price.tail.sale_price.text', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.plain_price.sale_price.text', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
                'modules/CDev/Sale/details/label.twig' => [
                    static::TO_DELETE => [
                        ['product.plain_price.tail.sale_price.text', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                    static::TO_ADD    => [
                        ['product.plain_price', 30, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ],
                ],
            ];
        }

        if (static::isModuleEnabled('CDev\SocialLogin')) {
            $templates['modules/CDev/SocialLogin/signin/signin.checkout.social.twig'] = [
                static::TO_ADD    => [
                    ['signin-anonymous-title', 5, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('XC\MultiVendor')) {
            $templates['modules/XC/MultiVendor/product/details/parts/vendor.twig'] = [
                static::TO_DELETE => [
                    ['center', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['product.details.page.info', 11, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('Amazon\PayWithAmazon')) {
            $templates['modules/Amazon/PayWithAmazon/login/signin/signin.checkout.twig'] = [
                static::TO_ADD    => [
                    ['signin-anonymous-title', 5, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }
        
        if (static::isModuleEnabled('CDev\Paypal')) {
            $templates['modules/CDev/Paypal/item_list/product/parts/express_checkout.twig'] = [
                static::TO_DELETE => [
                    ['itemsList.product.list.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.list.customer.right_info', 400, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }
                
        if (static::isModuleEnabled('QSL\MyWishlist')) {
            $templates['modules/QSL/MyWishlist/header/header.bar.wishlist.twig'] = [
                static::TO_DELETE => [
                    ['layout.header.bar', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header.line1', 150, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
            $templates['modules/QSL/MyWishlist/header/header.bar.links.logged.wishlist.twig'] = [
                static::TO_DELETE => [
                    ['layout.header.bar.links.logged', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
            
        }
        
        if (static::isModuleEnabled('QSL\ProductStickers')) {

            $templates['modules/QSL/ProductStickers/items_list/product/parts/common.product_label.twig'] = [
                static::TO_DELETE => [
                    ['itemsList.product.list.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.list.customer.photo', 40, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
            
        }
        if (static::isModuleEnabled('XC\ProductComparison') || \XLite\Core\Config::getInstance()->General->enable_add2cart_button_grid){
            $templates['items_list/product/parts/grid.buttons-container.twig'] = [
                static::TO_DELETE    => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        return $templates;
    }

    /**
     * @return array
     */
    protected static function moveClassesInLists()
    {
        $classes_list = [
            'XLite\View\TopCategories'  => [
                static::TO_DELETE => [
                    ['sidebar.first', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.header', 300, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'XLite\View\BannerRotation\BannerRotation'                                             => [
                static::TO_ADD    => [
                    ['layout.main.welcome.top', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],
            'XLite\View\Welcome'  => [
                static::TO_DELETE => [
                    ['center', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['center', 2, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ],            
        ];
        if (static::isModuleEnabled('XC\NewsletterSubscriptions')) {
            $classes_list['XLite\Module\XC\NewsletterSubscriptions\View\SubscribeBlock'] = [
                static::TO_DELETE => [
                    ['layout.main.footer.before',  \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['layout.main.footer.above_contacts', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('XC\ProductComparison')) {
            $classes_list['XLite\Module\XC\ProductComparison\View\AddToCompare\Products'] = [
                static::TO_DELETE => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.grid.customer.functional', 200, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
            $classes_list['XLite\Module\XC\ProductComparison\View\AddToCompare\ProductCompareIndicator'] = [
                static::TO_DELETE => [
                    ['layout.header.right', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['layout.header.right.mobile', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
               static::TO_ADD => [
                    ['layout.header.line1', 125, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('CDev\FeaturedProducts')) {
            $classes_list['XLite\Module\CDev\FeaturedProducts\View\Customer\FeaturedProducts'] = [
                static::TO_ADD    => [
                    ['welcome_tab_feat', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }
        
        if (static::isModuleEnabled('CDev\Bestsellers')) {
            $classes_list['XLite\Module\CDev\Bestsellers\View\Bestsellers'] = [
                static::TO_ADD    => [
                    ['welcome_tab_best', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('CDev\ProductAdvisor')) {
            $classes_list['XLite\Module\CDev\ProductAdvisor\View\NewArrivals'] = [
                static::TO_DELETE => [
                    ['sidebar.single', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['sidebar.second', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['center.bottom', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['welcome_tab_new', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
            ];
        }

        if (static::isModuleEnabled('QSL\MyWishlist')) {
            $classes_list['XLite\Module\QSL\MyWishlist\View\AddButtonItemsList'] = [
                static::TO_DELETE => [
                    ['itemsList.product.grid.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                    ['itemsList.product.list.customer.info', \XLite\Model\ViewList::INTERFACE_CUSTOMER],
                ],
                static::TO_ADD    => [
                    ['itemsList.product.grid.customer.functional', 100, \XLite\Model\ViewList::INTERFACE_CUSTOMER],  
                ],
            ];
        }

        return $classes_list;
    }

    /**
     * Returns image sizes
     *
     * @return array
     */
    public static function getImageSizes()
    {
        return [
            \XLite\Logic\ImageResize\Generator::MODEL_PRODUCT => [
                'SBSmallThumbnail' => [100, 100],
                'XSThumbnail'      => [90, 90],
            ],
        ];
    }    
}
