/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Categories list (carousel) script
 *
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */
function disableDragging_vb()
{
  var draggablePattern = '.vb-block .products-grid .product';
  jQuery(draggablePattern).draggable({ disabled: true }).removeClass('ui-state-disabled');
}

core.bind(
  'load',
  function() {
    decorate(
      'ProductsListView',
      'postprocess',
      function(isSuccess, initial)
      {
        arguments.callee.previousMethod.apply(this, arguments);
        disableDragging_vb();
      }
    )
  }
);

function vb_carousel() {
    jQuery(document).ready(function() {
        $(".vb-block .items-list").append('<a id="fc_prev_vb" class="fc-prev" style="display: block;"></a>');
        $(".vb-block .items-list").append('<a id="fc_next_vb" class="fc-next" style="display: block;"></a>');

        var owl = jQuery('.vb-block .products-grid');
        owl.owlCarousel({
            rewindNav : false,
            slideSpeed : 1000,
            pagination : false,
            responsiveBaseWidth: ".vb-block .products-grid",
            itemsCustom : [
                [0,2],
                [480,3],
                [768, 6]
            ],
            afterUpdate : vb_initControlsCallback,
            afterInit : vb_initControlsCallback
        });
        jQuery('#fc_prev_vb').click( function() {
          owl.trigger('owl.prev');
        });
        jQuery('#fc_next_vb').click( function() {
          owl.trigger('owl.next');
        });

        function vb_initControlsCallback() {
          if (this.owl.owlItems.length > this.owl.visibleItems.length) {
            jQuery('#fc_prev_vb,#fc_next_vb').show();
          } else {
            jQuery('#fc_prev_vb,#fc_next_vb').hide();
          }
        } 
    });
}

core.autoload(vb_carousel);