/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Categories list (carousel) script
 *
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */
function disableDragging_u()
{
  var draggablePattern = '.upselling-block .products-grid .product';
  jQuery(draggablePattern).draggable({ disabled: true }).removeClass('ui-state-disabled');
}

core.bind(
  'load',
  function() {
    decorate(
      'ProductsListView',
      'postprocess',
      function(isSuccess, initial)
      {
        arguments.callee.previousMethod.apply(this, arguments);
        disableDragging_u();
      }
    )
  }
);

function u_carousel() {
    jQuery(document).ready(function() {
        $(".upselling-block .items-list").append('<a id="fc_prev_u" class="fc-prev" style="display: block;"></a>');
        $(".upselling-block .items-list").append('<a id="fc_next_u" class="fc-next" style="display: block;"></a>');

        var owl = jQuery('.upselling-block .products-grid');
        owl.owlCarousel({
            rewindNav : false,
            slideSpeed : 1000,
            pagination : false,
            responsiveBaseWidth: ".upselling-block .products-grid",
            itemsCustom : [
                [0,2],
                [480,3],
                [768, 6]
            ],
            afterUpdate : u_initControlsCallback,
            afterInit : u_initControlsCallback
        });
        jQuery('#fc_prev_u').click( function() {
          owl.trigger('owl.prev');
        });
        jQuery('#fc_next_u').click( function() {
          owl.trigger('owl.next');
        });

        function u_initControlsCallback() {
          if (this.owl.owlItems.length > this.owl.visibleItems.length) {
            jQuery('#fc_prev_u,#fc_next_u').show();
          } else {
            jQuery('#fc_prev_u,#fc_next_u').hide();
          }
        }
    });
}

core.autoload(u_carousel);