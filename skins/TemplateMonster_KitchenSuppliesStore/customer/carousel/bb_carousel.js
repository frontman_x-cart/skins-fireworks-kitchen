/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Categories list (carousel) script
 *
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */
function disableDragging_bb()
{
  var draggablePattern = '.bb-block .products-grid .product';
  jQuery(draggablePattern).draggable({ disabled: true }).removeClass('ui-state-disabled');
}

core.bind(
  'load',
  function() {
    decorate(
      'ProductsListView',
      'postprocess',
      function(isSuccess, initial)
      {
        arguments.callee.previousMethod.apply(this, arguments);
        disableDragging_bb();
      }
    )
  }
);

function bb_carousel() {
    jQuery(document).ready(function() {
        $(".bb-block .items-list").append('<a id="fc_prev_bb" class="fc-prev" style="display: block;"></a>');
        $(".bb-block .items-list").append('<a id="fc_next_bb" class="fc-next" style="display: block;"></a>');

        var owl = jQuery('.bb-block .products-grid');
        owl.owlCarousel({
            rewindNav : false,
            slideSpeed : 1000,
            pagination : false,
            responsiveBaseWidth: ".bb-block .products-grid",
            itemsCustom : [
                [0,2],
                [480,3],
                [768, 6]
            ],
            afterUpdate : bb_initControlsCallback,
            afterInit : bb_initControlsCallback
        });
        jQuery('#fc_prev_bb').click( function() {
          owl.trigger('owl.prev');
        });
        jQuery('#fc_next_bb').click( function() {
          owl.trigger('owl.next');
        });

        function bb_initControlsCallback() {
          if (this.owl.owlItems.length > this.owl.visibleItems.length) {
            jQuery('#fc_prev_bb,#fc_next_bb').show();
          } else {
            jQuery('#fc_prev_bb,#fc_next_bb').hide();
          }
        }    
        
    });
}

core.autoload(bb_carousel);