function addImageParallax(selector, path, filename, width, height)
{
  var selector = $(selector);

  selector.addClass('parallax_section');
  selector.attr('data-type-media', 'image');
  selector.wrapInner('<div class="container parallax_content"></div>');
  selector.append('<div class="parallax_inner"><img class="parallax_media" src="'+path+filename+'" data-base-width="'+width+'" data-base-height="'+height+'"/></div>');

  selector.tmMediaParallax();
}

jQuery(window).load(function(){
  addImageParallax('#htmlcontent_home','skins/TemplateMonster_KitchenSuppliesStore/customer/images/parallax/','parallax.jpg','1920','815');
});