/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Controller
 *
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

jQuery(function() {
    $('.cycle-cloak.cycle-slideshow_').on('cycle-initialized', function(event, opts) {
        $(this).removeClass('.cycle-cloak');
    });
});

core.bind('block.product.details.postprocess', function(){
  if($('.product-details').closest('.product-quicklook').length) {
    $('.product-quicklook .product-image-gallery .cycle-slideshow_').attr('data-cycle-carousel-visible', 6);

    var images_count_q = $('.product-quicklook .product-image-gallery .cycle-slideshow_').data('images-count');

    if (images_count_q > 6) {
      $('.product-quicklook .product-image-gallery .cycle-slideshow_').cycle();
      $('.product-quicklook .product-image-gallery-navigation').removeClass('hidden');
    }    
  }

  init_product_gallery_carousel();
});

function init_product_gallery_carousel() {
  var cnt = 6;
  if (myViewport()['width'] < 480) {
    cnt = 2;
  } else if (myViewport()['width'] < 768) {
    cnt = 4;
  } else if (myViewport()['width'] < 992) {
    cnt = 3;
  } else if (myViewport()['width'] < 1200) {
    cnt = 4;
  } else  {
    cnt = 6;
  }

  $('.product-image-gallery .cycle-slideshow_').attr('data-cycle-carousel-visible', cnt);
  
  var images_count = $('.product-image-gallery .cycle-slideshow_').data('images-count');
  
  if (images_count > cnt) {
    $('.product-image-gallery .cycle-slideshow_').cycle();
    $('.product-image-gallery-navigation').removeClass('hidden');
  }
  
  $('.product-photo-box .product-photo').zoom();

}

function myViewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}