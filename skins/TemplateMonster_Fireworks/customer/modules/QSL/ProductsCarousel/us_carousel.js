/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Categories list (carousel) script
 *
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

function us_carousel() {
    jQuery(document).ready(function() {

        if (jQuery('.block-upselling.carousel-block').hasClass('buttonsOnly')) {
            var navigation = true;
            var pagination = false;
        } else {
            if (jQuery('.block-upselling.carousel-block').hasClass('paginationAndButtons')) {
                var navigation = true;
                var pagination = true;
            } else {
                var navigation = false;
                var pagination = true;
            }
        }

        if (jQuery("body").hasClass("skin-crisp_white-customer")) {
            var us_owl3_cw = jQuery('.skin-crisp_white-customer .block-upselling .products-grid');

            us_owl3_cw.owlCarousel({
                items : 3, //10 items above 1200px browser width
                itemsDesktop : [1200,2], //5 items between 1200px and 992px
                itemsDesktopSmall: [992, 1], // betweem 992px and 764px
                itemsTablet: [764,2], //2 items between 600 and 0
                itemsMobile : [480, 1], // itemsMobile disabled - inherit from itemsTablet option
                navigationText: false,
                pagination: pagination,
                navigation: navigation,
                mouseDrag:	false,
                touchDrag:	false,
                scrollPerPage: true,
                afterInit: prepeareDragging
            });
        } else {
            var us_owl3 = jQuery('.block-upselling .products-grid');

            us_owl3.owlCarousel({
                items: 4, //10 items above 1000px browser width
                itemsDesktop: [1199,3], //5 items between 1000px and 901px
                itemsDesktopSmall: [991,2],
                itemsTablet: [767,2], // betweem 900px and 601px
                itemsMobile : [480, 1], // itemsMobile disabled - inherit from itemsTablet option
                itemsScaleUp: true,
                navigationText: false,
                pagination: pagination,
                navigation: navigation,
                mouseDrag:	false,
                touchDrag:	false,
                scrollPerPage: true,
                afterInit: prepeareDragging
            });
        }


    });
}

core.autoload(us_carousel);