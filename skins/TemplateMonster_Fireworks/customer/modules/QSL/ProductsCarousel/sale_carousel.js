/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Categories list (carousel) script
 *
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

function sale_carousel() {
    jQuery(document).ready(function() {

        if (jQuery('.block-sale-products.carousel-block').hasClass('buttonsOnly')) {
            var navigation = true;
            var pagination = false;
        } else {
            if (jQuery('.block-sale-products.carousel-block').hasClass('paginationAndButtons')) {
                var navigation = true;
                var pagination = true;
            } else {
                var navigation = false;
                var pagination = true;
            }
        }

        if (jQuery("body").hasClass("skin-crisp_white-customer")) {
            var owl_sale_cw = jQuery('.skin-crisp_white-customer.one-sidebar #content .block-sale-products .products-grid');
            var owl_sale2_cw = jQuery('.skin-crisp_white-customer.no-sidebars .block-sale-products .products-grid');
            var owl_sale3_cw = jQuery('.skin-crisp_white-customer.two-sidebars #content .block-sale-products .products-grid');

            owl_sale_cw.owlCarousel({
                items : 3, //10 items above 1200px browser width
                itemsDesktop : [1200,2], //5 items between 1200px and 992px
                itemsDesktopSmall: [992, 1], // betweem 992px and 764px
                itemsTablet: [764,2], //2 items between 600 and 0
                itemsMobile : [480, 1], // itemsMobile disabled - inherit from itemsTablet option
                navigationText: false,
                pagination: pagination,
                navigation: navigation,
                mouseDrag:	false,
                touchDrag:	false,
                scrollPerPage: true,
                afterInit: prepeareDragging
            });


            owl_sale2_cw.owlCarousel({
                items: 4, //10 items above 1000px browser width
                itemsDesktop: [1200,3], //5 items between 1000px and 901px
                itemsDesktopSmall: [992,2],
                itemsTablet: [764,2], // betweem 900px and 601px
                itemsMobile : [480, 1], // itemsMobile disabled - inherit from itemsTablet option
                itemsScaleUp: true,
                navigationText: false,
                pagination: pagination,
                navigation: navigation,
                mouseDrag:	false,
                touchDrag:	false,
                scrollPerPage: true,
                afterInit: prepeareDragging
            });

            owl_sale3_cw.owlCarousel({
                items: 1, //10 items above 1000px browser width
                itemsDesktop: [1200,1], //5 items between 1000px and 901px
                itemsDesktopSmall: [992,1],
                itemsTablet: [764,2], // betweem 900px and 601px
                itemsMobile : [480, 1], // itemsMobile disabled - inherit from itemsTablet option
                itemsScaleUp: true,
                navigationText: false,
                pagination: pagination,
                navigation: navigation,
                mouseDrag:	false,
                touchDrag:	false,
                scrollPerPage: true,
                afterInit: prepeareDragging
            });

        } else {
            var owl_sale = jQuery('.one-sidebar #content .block-sale-products .products-grid');
            var owl_sale2 = jQuery('.no-sidebars .block-sale-products .products-grid');
            var owl_sale3 = jQuery('.two-sidebars #content .block-sale-products .products-grid');

            owl_sale.owlCarousel({
                items : 3, //10 items above 1000px browser width
                itemsDesktop : [1199,2], //5 items between 1000px and 901px
                itemsDesktopSmall: [991, 2], // betweem 900px and 601px
                itemsTablet: [767,2], //2 items between 600 and 0
                itemsMobile : [480, 1], // itemsMobile disabled - inherit from itemsTablet option
                navigationText: false,
                pagination: pagination,
                navigation: navigation,
                mouseDrag:	false,
                touchDrag:	false,
                scrollPerPage: true,
                afterInit: prepeareDragging
            });

            owl_sale2.owlCarousel({
                items: 4, //10 items above 1000px browser width
                itemsDesktop: [1199,3], //5 items between 1000px and 901px
                itemsDesktopSmall: [991,3],
                itemsTablet: [767,2], // betweem 900px and 601px
                itemsMobile : [480, 1], // itemsMobile disabled - inherit from itemsTablet option
                itemsScaleUp: true,
                navigationText: false,
                pagination: pagination,
                navigation: navigation,
                mouseDrag:	false,
                touchDrag:	false,
                scrollPerPage: true,
                afterInit: prepeareDragging
            });

            owl_sale3.owlCarousel({
                items: 2, //10 items above 1000px browser width
                itemsDesktop: [1199,1], //5 items between 1000px and 901px
                itemsDesktopSmall: [991,1],
                itemsTablet: [767,1], // betweem 900px and 601px
                itemsMobile : [480, 1], // itemsMobile disabled - inherit from itemsTablet option
                itemsScaleUp: true,
                navigationText: false,
                pagination: pagination,
                navigation: navigation,
                mouseDrag:	false,
                touchDrag:	false,
                scrollPerPage: true,
                afterInit: prepeareDragging
            });
        }


    });
}

core.autoload(sale_carousel);